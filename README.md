CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Accessibility Scanner module allows you to use Drupal in combination with
[Achecker](https://achecker.ca) to perform web accessibility scans on local and remote websites based of a list of URLs or XML sitemaps, all within the familiar Drupal admin interface.

 * For a full description of the module, visit the [project page](https://www.drupal.org/project/accessibility_scanner)
   or visit [Getting Started with Accessibility Scanner](https://www.drupal.org/node/3042500)

 * [To submit bug reports and feature suggestions, or to track changes](https://drupal.org/project/issues/accessibility_scanner)

REQUIREMENTS
------------

This module requires the following modules:

 * [Key](https://www.drupal.org/project/key), which provides the ability to manage sensitive keys
 * [Web Page Archive](https://www.drupal.org/project/web_page_archive), allows you to perform periodic snapshots and visual regression testing
   - Please note: Web Page Archive module requires **Composer** for installation as
     the package mtdowling/cron-expression can be not installed by default
     installation.
   - We recommend using Composer to install Key, Web Page Archive, and Accessibility Scanner.


INSTALLATION
------------

Install the Accessibility Scanner module as you would normally install a
contributed Drupal module. [Further information on installing modules](https://www.drupal.org/node/1897420).


CONFIGURATION
-------------

 * Permissions
   - To follow along with this guide, you will need the following permissions:
      - administer web page archive
      - view web page archive results
      - administer keys

 * When you first install the module, you will need to configure your AChecker
   credentials.
   - First, we need to use the Key module for managing the AChecker web service
     id.
   To do so, go to Configuration -> Keys and then click the Add Key button
   (or just navigate to /admin/config/system/keys/add).
   Please review the key module documentation for best practices on key
   management.
   - Next, we need to tell accessibility scanner where to find the web service
     id.
   Go to Configuration -> Web Page Archive -> Settings
   (or just navigate to /admin/config/system/web-page-archive/settings).

 * Configuring a Scanner Job
   - Familiarize yourself with creating capture jobs in Web Page Archive.
     When you get to the Configuring Capture Utilities section,
     specify Achecker Accessibility Scanner and then click the Add button.
   - Next learn about running capture jobs on Web Page Archive.
   - Upon run completion you should see scan results,
     but refer to viewing capture job results in Web Page Archive, if you need
     more context.
      - Preview Mode - Provides an overview of scan results (e.g. pass/fail,
        number of errors, guidelines used, etc)
      - Full Mode - More detailed scan results on a per-URL basis


MAINTAINERS
-----------

Current maintainers:
 * [David Stinemetze (WidgetsBurritos)](https://www.drupal.org/u/widgetsburritos)
 * [Paul Maddern (pobster)](https://www.drupal.org/u/pobster)
 * [Adrianna Flores (vessel_adrift)](https://www.drupal.org/u/vessel_adrift)

This project has been supported by:
 * [Rackspace Hosting](https://www.drupal.org/rackspace-hosting) provided development resources
