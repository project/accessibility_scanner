<?php

/**
 * @file
 * Post Update commands for accessibility_scanner.
 */

use Drupal\Core\Config\FileStorage;

/**
 * Issue 3201204: Installs the default @axe-core/cli config.
 */
function accessibility_scanner_post_update_3201204_install_axecore_cli_capture() {
  $path = drupal_get_path('module', 'accessibility_scanner') . '/config/install';
  $source = new FileStorage($path);
  $config_storage = \Drupal::service('config.storage');
  $config_name = 'web_page_archive.wpa_axecore_cli_capture.settings';
  $config_storage->write($config_name, $source->read($config_name));
}
