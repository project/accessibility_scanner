/**
 * @file
 * Provides some very basic filtering functionality for achecker results.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.accessibilityScanner = Drupal.accessibilityScanner || {};
  Drupal.accessibilityScanner.axeCoreCli = {
    drawCharts: function () {
      Drupal.accessibilityScanner.axeCoreCli.drawPreviewChart();
    },
    drawPreviewChart: function () {
      var charts = document.getElementsByClassName('axecore-cli-summary__chart');
      for (var i = 0; i < charts.length; i++) {
        var chart = charts[i];
        var total = +chart.dataset.violations + +chart.dataset.passes + +chart.dataset.incomplete;
        var data = [
          [Drupal.t('Type'), Drupal.t('Quantity'), { role: 'style' }],
          [Drupal.t('Violations'), +chart.dataset.violations, '#cc3232'],
          [Drupal.t('Passing'), +chart.dataset.passes, '#e7b416'],
          [Drupal.t('Incomplete'), +chart.dataset.incomplete, '#2dc937'],
        ];

        var dataTable = google.visualization.arrayToDataTable(data);
        var chart = new google.visualization.PieChart(chart);
        var options = {
          annotations: {
            alwaysOutside: true,
            textStyle: {
              fontSize: 16,
              auraColor: 'none',
              color: '#555'
            },
          },
          chart: {
            title: '',
            subtitle: '',
          },
          chartArea:{left:0,top:0,bottom:0,top:0, width: '100%'},
          colors: ['#cc3232', '#2dc937', '#e7b416'],
        };

        chart.draw(dataTable, options);
      }
    }

  }

  Drupal.behaviors.accessibilityScannerAxeCoreCliGoogleChart = {
    attach: function attach(context) {
      google.charts.load('current', {packages: ['corechart', 'bar']});
      google.charts.setOnLoadCallback(Drupal.accessibilityScanner.axeCoreCli.drawCharts);
    }
  };

})(jQuery, Drupal, drupalSettings);
