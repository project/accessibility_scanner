<?php

namespace Drupal\accessibility_scanner\Plugin\CaptureResponse;

use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\web_page_archive\Plugin\CaptureResponseInterface;
use Drupal\web_page_archive\Plugin\CaptureResponse\UriCaptureResponse;

/**
 * The @axe-core/cli capture response.
 */
class AxeCoreCliCaptureResponse extends UriCaptureResponse {

  /**
   * {@inheritdoc}
   */
  public static function getId() {
    return 'wpa_axecore_cli_capture_response';
  }

  /**
   * {@inheritdoc}
   */
  public function renderable(array $options = []) {
    return (isset($options['mode']) && $options['mode'] == 'full') ?
      $this->renderFull($options) : $this->renderPreview($options);
  }

  /**
   * Renders "preview" mode.
   */
  private function renderPreview(array $options) {
    $contents = $this->retrieveFileContents();

    $route_params = [
      'web_page_archive_run_revision' => $options['vid'],
      'delta' => $options['delta'],
    ];

    $render = [
      '#theme' => 'wpa-axecore-cli-preview',
      '#summary' => $this->generateSummary($contents),
      '#url' => $this->captureUrl,
      '#view_button' => [
        '#type' => 'link',
        '#url' => Url::fromRoute('entity.web_page_archive.modal', $route_params),
        '#title' => $this->t('View Detailed Report'),
        '#attributes' => [
          'class' => ['button', 'use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 1280]),
        ],
      ],
    ];

    return $render;
  }

  /**
   * Retrieves file contents.
   */
  public function retrieveFileContents() {
    if (!empty($this->content) && file_exists($this->content)) {
      $contents = file_get_contents($this->content);
      return Json::decode($contents);
    }
    return '';
  }

  /**
   * Summarize contents.
   */
  public function generateSummary($contents) {
    $ret = [
      'tags' => !empty($contents[0]['toolOptions']['runOnly']['values']) ? implode(', ', $contents[0]['toolOptions']['runOnly']['values']) : '',
    ];
    $types = ['violations', 'inapplicable', 'incomplete', 'passes'];

    foreach ($types as $type) {
      $ret["num_distinct_{$type}"] = 0;
      $ret["num_total_{$type}"] = 0;
      if (!empty($contents[0][$type])) {
        foreach ($contents[0][$type] as $instance) {
          $ret["num_distinct_{$type}"]++;
          $ret["num_total_{$type}"] += count($instance['nodes']);
        }
      }
    }

    // If there are no violations then we are "passing".
    $ret['is_passing'] = empty($ret['num_total_violations']);

    return $ret;
  }

  /**
   * Renders full mode.
   */
  private function renderFull(array $options) {
    $contents = $this->retrieveFileContents();

    $render = [
      '#theme' => 'wpa-axecore-cli-full-report',
      '#summary' => $this->generateSummary($contents),
      '#results' => $contents[0],
    ];

    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public static function compare(CaptureResponseInterface $a, CaptureResponseInterface $b, array $compare_utilities, array $tags = [], array $data = []) {
    $tags[] = '@axe-core/cli';
    return parent::compare($a, $b, $compare_utilities, $tags, $data);
  }

}
