<?php

namespace Drupal\Tests\accessibility_scanner\Unit\Plugin\CaptureResponse;

use Drupal\Tests\UnitTestCase;
use Drupal\accessibility_scanner\Plugin\CaptureResponse\AcheckerCaptureResponse;

/**
 * Tests the AcheckerCaptureResponse class.
 *
 * @group accessibility_scanner
 */
class AcheckerCaptureResponseTest extends UnitTestCase {

  /**
   * Test get AcheckerCaptureResponse::stripInvalidHtmlEntitiesFromXmlFile().
   */
  public function testStripInvalidHtmlEntitiesFromXmlFileStripsExpectedEntities() {
    $response = new AcheckerCaptureResponse('/some/path/to/file.xml', 'http://www.somesite.com');
    $original = '&rsquo;abc&nbsp;def&lt;p&gt;something&rsquo;other nbsp';
    $expected = '&#8217;abc def&lt;p&gt;something&#8217;other nbsp';
    $actual = $response->stripInvalidHtmlEntitiesFromXmlFile($original);
    $this->assertEquals($expected, $actual);
  }

}
