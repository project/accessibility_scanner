<?php

namespace Drupal\Tests\accessibility_scanner\Unit\Plugin\CaptureResponse;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Tests\UnitTestCase;
use Drupal\accessibility_scanner\Plugin\CaptureResponse\AxeCoreCliCaptureResponse;

/**
 * Tests the AxeCoreCliCaptureResponse class.
 *
 * @group accessibility_scanner
 * @requires module web_page_archive
 */
class AxeCoreCliCaptureResponseTest extends UnitTestCase {

  use StringTranslationTrait;

  /**
   * Test get AxeCoreCliCaptureResponse::retrieveFileContents().
   */
  public function testRetrieveFileContentsLoadsValidJson() {
    $response = new AxeCoreCliCaptureResponse(__DIR__ . '/../../../../fixtures/axe-failing.json', 'http://www.drupal.org');
    $actual = $response->retrieveFileContents();
    $this->assertNotEmpty($actual[0]['inapplicable']);
    $this->assertNotEmpty($actual[0]['violations']);
    $this->assertNotEmpty($actual[0]['passes']);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::retrieveFileContents().
   */
  public function testRetrieveFileContentsDoesntLoadInvalidJson() {
    $response = new AxeCoreCliCaptureResponse(__DIR__ . '/../../../../fixtures/axe-invalid.json', 'http://www.drupal.org');
    $actual = $response->retrieveFileContents();
    $this->assertEmpty($actual);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::generateSummary().
   */
  public function testGenerateSummaryPopulatesExpectedEmptyData() {
    $response = new AxeCoreCliCaptureResponse(__DIR__ . '/../../../../fixtures/axe-failing.json', 'http://www.drupal.org');
    $actual = $response->generateSummary([]);
    $expected = [
      'tags' => '',
      'num_distinct_violations' => 0,
      'num_total_violations' => 0,
      'num_distinct_inapplicable' => 0,
      'num_total_inapplicable' => 0,
      'num_distinct_incomplete' => 0,
      'num_total_incomplete' => 0,
      'num_distinct_passes' => 0,
      'num_total_passes' => 0,
      'is_passing' => TRUE,
    ];
    $this->assertEquals($expected, $actual);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::generateSummary().
   */
  public function testGenerateSummaryPopulatesExpectedPassingMockData() {
    $response = new AxeCoreCliCaptureResponse(__DIR__ . '/../../../../fixtures/axe-passing.json', 'http://www.drupal.org');
    $contents = $response->retrieveFileContents();
    $actual = $response->generateSummary($contents);
    $expected = [
      'tags' => 'wcag21aa',
      'num_distinct_violations' => 0,
      'num_total_violations' => 0,
      'num_distinct_inapplicable' => 1,
      'num_total_inapplicable' => 0,
      'num_distinct_incomplete' => 0,
      'num_total_incomplete' => 0,
      'num_distinct_passes' => 1,
      'num_total_passes' => 1,
      'is_passing' => TRUE,
    ];
    $this->assertEquals($expected, $actual);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::generateSummary().
   */
  public function testGenerateSummaryPopulatesExpectedFailingMockData() {
    $response = new AxeCoreCliCaptureResponse(__DIR__ . '/../../../../fixtures/axe-failing.json', 'http://www.drupal.org');
    $contents = $response->retrieveFileContents();
    $actual = $response->generateSummary($contents);
    $expected = [
      'tags' => 'wcag21aa, best-practice',
      'num_distinct_violations' => 2,
      'num_total_violations' => 10,
      'num_distinct_inapplicable' => 18,
      'num_total_inapplicable' => 0,
      'num_distinct_incomplete' => 1,
      'num_total_incomplete' => 1,
      'num_distinct_passes' => 12,
      'num_total_passes' => 30,
      'is_passing' => FALSE,
    ];
    $this->assertEquals($expected, $actual);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::renderable().
   */
  public function testRenderablePassingPreview() {
    $options = [
      'mode' => 'preview',
      'vid' => 20,
      'delta' => 3,
    ];
    $response = new AxeCoreCliCaptureResponse(__DIR__ . '/../../../../fixtures/axe-passing.json', 'http://www.drupal.org');
    $response->setStringTranslation($this->getStringTranslationStub());
    $this->setStringTranslation($this->getStringTranslationStub());
    $expected = [
      '#theme' => 'wpa-axecore-cli-preview',
      '#summary' => [
        'tags' => 'wcag21aa',
        'num_distinct_violations' => 0,
        'num_total_violations' => 0,
        'num_distinct_inapplicable' => 1,
        'num_total_inapplicable' => 0,
        'num_distinct_incomplete' => 0,
        'num_total_incomplete' => 0,
        'num_distinct_passes' => 1,
        'num_total_passes' => 1,
        'is_passing' => TRUE,
      ],
      '#url' => 'http://www.drupal.org',
      '#view_button' => [
        '#type' => 'link',
        '#url' => new Url('entity.web_page_archive.modal', [
          'delta' => 3,
          'web_page_archive_run_revision' => 20,
        ]),
        '#title' => $this->t('View Detailed Report'),
        '#attributes' => [
          'class' => ['button', 'use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 1280]),
        ],
      ],
    ];
    $actual = $response->renderable($options);
    $this->assertEquals($expected, $actual);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::renderable().
   */
  public function testRenderableFailingPreview() {
    $options = [
      'mode' => 'preview',
      'vid' => 20,
      'delta' => 3,
    ];
    $response = new AxeCoreCliCaptureResponse(__DIR__ . '/../../../../fixtures/axe-failing.json', 'http://www.drupal.org');
    $response->setStringTranslation($this->getStringTranslationStub());
    $this->setStringTranslation($this->getStringTranslationStub());
    $expected = [
      '#theme' => 'wpa-axecore-cli-preview',
      '#summary' => [
        'tags' => 'wcag21aa, best-practice',
        'num_distinct_violations' => 2,
        'num_total_violations' => 10,
        'num_distinct_inapplicable' => 18,
        'num_total_inapplicable' => 0,
        'num_distinct_incomplete' => 1,
        'num_total_incomplete' => 1,
        'num_distinct_passes' => 12,
        'num_total_passes' => 30,
        'is_passing' => FALSE,
      ],
      '#url' => 'http://www.drupal.org',
      '#view_button' => [
        '#type' => 'link',
        '#url' => new Url('entity.web_page_archive.modal', [
          'delta' => 3,
          'web_page_archive_run_revision' => 20,
        ]),
        '#title' => $this->t('View Detailed Report'),
        '#attributes' => [
          'class' => ['button', 'use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 1280]),
        ],
      ],
    ];
    $actual = $response->renderable($options);
    $this->assertEquals($expected, $actual);
  }

}
