<?php

namespace Drupal\Tests\accessibility_scanner\Unit\Plugin\CaptureUtility;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\UnitTestCase;
use Drupal\accessibility_scanner\Plugin\CaptureUtility\AxeCoreCliCaptureUtility;

/**
 * Tests the AxeCoreCliCaptureResponse class.
 *
 * @group accessibility_scanner
 * @requires module web_page_archive
 */
class AxeCoreCliCaptureUtilityTest extends UnitTestCase {

  use StringTranslationTrait;

  /**
   * Helper function to retrieve a mock capture utility.
   */
  public function getMockCaptureUtility($system_config = []) {
    $configuration = [];
    $plugin_id = '';
    $plugin_definition = [];
    $logger = $this->getMockBuilder('\Psr\Log\LoggerInterface')
      ->getMock();
    $config = $this->getMockBuilder('\Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $config->expects($this->any())
      ->method('get')
      ->will($this->returnValue($system_config));
    $config_factory = $this->getMockBuilder('\Drupal\Core\Config\ConfigFactoryInterface')
      ->getMock();
    $config_factory->expects($this->any())
      ->method('get')
      ->will($this->returnValue($config));
    $file_system = $this->getMockBuilder('\Drupal\Core\File\FileSystemInterface')
      ->getMock();
    return new AxeCoreCliCaptureUtility($configuration, $plugin_id, $plugin_definition, $logger, $config_factory, $file_system);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::checkValidAxeBinary().
   */
  public function testCheckValidAxeBinaryThrowsExceptionWithNoFileSpecified() {
    $capture_utility = $this->getMockCaptureUtility();

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('No @axe-core/cli binary provided.');
    $capture_utility->checkValidAxeBinary();
  }

  /**
   * Test get AxeCoreCliCaptureResponse::checkValidAxeBinary().
   */
  public function testCheckValidAxeBinaryThrowsExceptionWithMissingFile() {
    $capture_utility = $this->getMockCaptureUtility([
      'axecore_cli_binary' => '/file/does/not/exist',
    ]);

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Provided @axe-core/cli binary does not exist. Please check the specified path is correct.');
    $capture_utility->checkValidAxeBinary();
  }

  /**
   * Test get AxeCoreCliCaptureResponse::checkValidAxeBinary().
   */
  public function testCheckValidAxeBinaryThrowsExceptionWithNonExecutableFile() {
    $f = tmpfile();
    $file = stream_get_meta_data($f)['uri'];
    $capture_utility = $this->getMockCaptureUtility([
      'axecore_cli_binary' => $file,
    ]);

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Provided @axe-core/cli binary is not executable. Please check permissions.');
    $capture_utility->checkValidAxeBinary();
    fclose($f);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::checkValidAxeBinary().
   */
  public function testCheckValidAxeBinaryDoesNotThrowExceptionWithExecutableFile() {
    $f = tmpfile();
    $file = stream_get_meta_data($f)['uri'];
    chmod($file, 0755);
    $capture_utility = $this->getMockCaptureUtility([
      'axecore_cli_binary' => $file,
    ]);

    $this->assertNotNull($capture_utility->checkValidAxeBinary());
    fclose($f);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::checkValidAxeBinary().
   */
  public function testCheckValidAxeBinaryThrowsExceptionWithMissingChecksum() {
    $f = tmpfile();
    $file = stream_get_meta_data($f)['uri'];
    chmod($file, 0755);
    $capture_utility = $this->getMockCaptureUtility([
      'axecore_cli_binary' => $file,
      'axecore_cli_binary_verify_checksum' => TRUE,
      'axecore_cli_binary_checksum' => '',
    ]);

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Provided @axe-code/cli binary does not match expected checksum: ');
    $capture_utility->checkValidAxeBinary();
    fclose($f);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::checkValidAxeBinary().
   */
  public function testCheckValidAxeBinaryThrowsExceptionWithMismatchingChecksum() {
    $f = tmpfile();
    $file = stream_get_meta_data($f)['uri'];
    chmod($file, 0755);
    $capture_utility = $this->getMockCaptureUtility([
      'axecore_cli_binary' => $file,
      'axecore_cli_binary_verify_checksum' => TRUE,
      'axecore_cli_binary_checksum' => 'abc123',
    ]);

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Provided @axe-code/cli binary does not match expected checksum: abc123');
    $capture_utility->checkValidAxeBinary();
    fclose($f);
  }

  /**
   * Test get AxeCoreCliCaptureResponse::checkValidAxeBinary().
   */
  public function testCheckValidAxeBinaryDoesNotThrowExceptionWithMatchingChecksum() {
    $f = tmpfile();
    $file = stream_get_meta_data($f)['uri'];
    chmod($file, 0755);
    $capture_utility = $this->getMockCaptureUtility([
      'axecore_cli_binary' => $file,
      'axecore_cli_binary_verify_checksum' => TRUE,
      'axecore_cli_binary_checksum' => md5_file($file),
    ]);

    $this->assertNotNull($capture_utility->checkValidAxeBinary());
    fclose($f);
  }

  /**
   * Test AxeCoreCliCaptureUtility::encodeSelectors().
   */
  public function testSelectorsAreProperlyEncoded() {
    $string = implode(PHP_EOL, [
      '        ',
      '.abc',
      ' .def ',
      '.ghi, .jkl ,.mno',
      '.pq .rs',
      '',
      PHP_EOL . '  .tuv  ' . PHP_EOL,
    ]);
    $capture_utility = $this->getMockCaptureUtility();
    $expected = '.abc,.def,.ghi,.jkl,.mno,.pq .rs,.tuv';
    $this->assertEquals($expected, $capture_utility->encodeSelectors($string));
  }

  /**
   * Test AxeCoreCliCaptureUtility::decodeSelectors().
   */
  public function testSelectorsAreProperlyDecoded() {
    $string = '   .abc, .def ,.ghi,.jkl,.mno,.pq .rs,' . PHP_EOL . '.tuv   ';
    $capture_utility = $this->getMockCaptureUtility();
    $expected = implode(PHP_EOL, [
      '.abc',
      '.def',
      '.ghi',
      '.jkl',
      '.mno',
      '.pq .rs',
      '.tuv',
    ]);
    $this->assertEquals($expected, $capture_utility->decodeSelectors($string));
  }

}
